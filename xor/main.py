import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time as time

from utils import callback_print_histories

np.random.seed(int(time.time()))

model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(3, input_dim=2, activation='relu'))
model.add(tf.keras.layers.Dense(5, activation='relu'))
model.add(tf.keras.layers.Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer=tf.keras.optimizers.Adam(lr=0.075), metrics=['accuracy'])

model.summary()

dataset = np.loadtxt('data.csv', delimiter=',')
X = dataset[:, 0:2]
Y = dataset[:, 2]

history = model.fit(X, Y, epochs=500, validation_split=0.3, callbacks=[callback_print_histories.clazz()])

# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# evaluate the model
scores = model.evaluate(X, Y)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
